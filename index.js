const express = require("express");
const app = express();
const http = require("http").Server(app);
const path = require('path');
const socketIO = require("socket.io")(http);


app.use(express.static(path.resolve(__dirname, './client/build')));
let users = [];

socketIO.on("connection", (socket) => {
  console.log(`⚡: ${socket.id} user just connected!`);

  socket.on("newUser", (data) => {
    users.push(data);
    socketIO.emit("newUserResponse", users);
  });

  socket.on("disconnect", () => {
    console.log("🔥: A user disconnected");
    users = users.filter((user) => user.socketID !== socket.id);
    socketIO.emit("newUserResponse", users);
    socket.disconnect();
  });
});

http.listen(process.env.PORT || 3000, () => {
  console.log(`Server listening on ${3000}`);
});
app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, './client/build', 'index.html'));
});