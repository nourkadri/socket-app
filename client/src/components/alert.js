import * as React from "react";
import Button from "@mui/material/Button";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";

export default function Alert(props) {
  const Alert = React.forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
  });
  const handleClose = () => {
    props.setOpen(false);
  };
  return (
    <div>
      <Snackbar
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        open={props.open}
        key={"top" + "center"}
        autoHideDuration={3000}
        onClose={handleClose} 
      >
        <Alert onClose={handleClose} sx={{ width: "100%",backgroundColor: "red"}}>
        Like by {props.likedBy}
        </Alert>
      </Snackbar>
    </div>
  );
}
