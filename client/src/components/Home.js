import React, {useState} from 'react'
import {useNavigate} from "react-router-dom"
import Picker from 'emoji-picker-react';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';


const Home = ({socket}) => {
    const navigate = useNavigate()
    const [userName, setUserName] = useState("")
    const [enable, setEnable] = useState(true)
    const [type, setType] = useState(1)
    const [chosenEmoji, setChosenEmoji] = useState(null);

    const onEmojiClick = (event, emojiObject) => {
      setChosenEmoji(emojiObject);
      setEnable(false)

    };
    const handleSubmit = (e) => {
      const em=chosenEmoji.emoji
        e.preventDefault()
        localStorage.setItem("userName", userName)
        socket.emit("newUser", {userName, socketID: socket.id,em,type})
        navigate("/users")
    }
   
  return (
    <form className='home__container' onSubmit={handleSubmit}>
        <h2 className='home__header'>Write your name and select emoji to join</h2>
        <label htmlFor="username">Username</label>
        <input type="text" 
        minLength={4} 
        name="username" 
        id='username'
        className='username__input' 
        value={userName} 
        onChange={e => setUserName(e.target.value)}
        />
         <FormControl onChange={e=>setType(e.target.value)}>
      <RadioGroup
        aria-labelledby="demo-radio-buttons-group-label"
        name="radio-buttons-group"
      >
        <FormControlLabel value="1" control={<Radio />} label="Frontend" />
        <FormControlLabel value="2" control={<Radio />} label="Backend" />
        <FormControlLabel value="3" control={<Radio />} label="Both" />
      </RadioGroup>
    </FormControl>
           <div>
      <Picker onEmojiClick={onEmojiClick} />
    </div>
        <button disabled={enable} className='home__cta'>JOIN</button>
    </form>
  )
}

export default Home