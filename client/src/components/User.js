import React, { useState, useEffect } from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import Zoom from "@mui/material/Zoom";
import { useNavigate } from "react-router-dom";
import IconButton from "@mui/material/IconButton";
import ThumbUpAltIcon from "@mui/icons-material/ThumbUpAlt";
import Alert from "./alert";
import sound from "./sound.wav";
const User = ({ socket }) => {
  const audio = new Audio(sound);
  const userLocal = localStorage.getItem("userName");
  const navigate = useNavigate();
  const [dataLike, setDataLike] = useState([]);
  const [open, setOpen] = useState();
  const [likedBy, setLikedBy] = useState();
  const leave = () => {
    localStorage.removeItem("userName");
    navigate("/");
    window.location.reload();
  };

  const like = (data) => {
    socket.emit("like", [{ user: userLocal, liked: data }]);
  };

  const [users, setUsers] = useState([]);
  useEffect(() => {
    socket.on("newUserResponse", (data) => setUsers(data));
  }, [socket, users]);

  useEffect(() => {
    socket.on("likeResponse", (data) => setDataLike(data));
    if (dataLike.length !== 0) {
      if (dataLike[0].liked === userLocal) {
        audio.play();
        setLikedBy(dataLike[0].user);
        setOpen(true);
      }
    }
  }, [dataLike, socket]);

  return (
    <div>
      <Alert open={open} setOpen={setOpen} likedBy={likedBy} />
      <header className="__mainHeader">
        <p>Welcome</p>
        <button className="leave__btn" onClick={leave}>
          LEAVE
        </button>
      </header>
      <div className="chat__sidebar">
        <div>
          <h4 className="chat__header">ACTIVE USERS</h4>
          <div className="online__users">
            {users.map((user) => (
              <Zoom
                key={user.socketID}
                in={true}
                style={{ transitionDelay: "500ms" }}
              >
                <Card className="card" sx={{ minWidth: 275 }}>
                  <CardContent className="card__content">
                    <Typography
                      className="emj"
                      sx={{ fontSize: 100 }}
                      gutterBottom
                    >
                      {user.em}
                    </Typography>
                    <Typography className="emj" variant="h3" component="div">
                      {user.userName}
                      <div className="type">
                        {user.type == 1 ? <h5>Frontend</h5> : null}
                        {user.type == 2 ? <h5>Backend</h5> : null}
                        {user.type == 3 ? <h5>Both</h5> : null}
                      </div>
                    </Typography>
                  </CardContent>
                  {user.userName != userLocal ? (
                    <div className="btn_like">
                      <IconButton
                        aria-label="like"
                        onClick={(e) => like(user.userName)}
                        color="primary"
                      >
                        <ThumbUpAltIcon sx={{ fontSize: 50 }} />
                      </IconButton>
                    </div>
                  ) : null}
                </Card>
              </Zoom>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default User;
